# examples

This repository contains examples of how to use the _uunet_ library, which contains code for the analysis and 
mining of networks produced by the Uppsala University Information Laboratory (https://infolab.it.uu.se).

## Requirements

To install, use and modify the library you need:

* A recent version of git.
* The *cmake* build system.
* A modern, C++14-ready compiler.
* The _uunet_ library installed in your system.

## Installation

To download the library:

```sh
git clone https://bitbucket.org/uuinfolab/example.git
cd example
```

Then create a build directory and set it up for compilation:

```sh
mkdir build
cd build
cmake ..
```

Now you can compile all the examples:

```sh
make
```

Alternatively, if you want to compile only one example, run _make_ followed by the example to be compiled, e.g.:

```sh
make graphs
```
You can now execute the examples you have compiled:

```sh
./graphs
```

## Available examples

*cubes*: basic functionality of VCubes and ECubes, which are the datastructures based on which all network
data models are built.

*graphs*: basic graph theory concepts: types of graphs, terminology, degree, graph operations. 
