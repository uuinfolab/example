/**
 * Basic graph operations.
 */

#include "networks/Network.hpp" // Network
#include "networks/MultiNetwork.hpp" // MultiNetwork
#include "generation/standard_graphs.hpp" // special graphs (complete, wheel, etc.)
#include "measures/degree.hpp" // degree, degree_sequence, degree_distribution, average_degree
#include "operations/subgraph.hpp" // vertex_induced_subgraph
#include "operations/union.hpp" // graph_union
#include "operations/intersection.hpp" // graph_intersection
#include "operations/complement.hpp" // graph_complement
#include "operations/subdivision.hpp" // edge_subdivision
#include "operations/contraction.hpp" // edge_contraction
#include "utils/summary.hpp" // summary_short
#include "objects/Walk.hpp" // Walk
#include "objects/Trail.hpp" // Trail
#include "objects/Path.hpp" // Path

int
main()
{
    
    using namespace uu::net;
    using namespace uu::core;
    
    std::cout << "Creating a simple graph and a multigraph." << std::endl;
    
    auto g = std::make_unique<Network>("G", EdgeDir::UNDIRECTED, LoopMode::DISALLOWED);
    auto m = std::make_unique<MultiNetwork>("M", EdgeDir::UNDIRECTED, LoopMode::ALLOWED);
    
    std::cout << "Adding vertices and edges." << std::endl;
    
    auto v1 = g->vertices()->add("v1");
    auto v2 = g->vertices()->add("v2");
    auto v3 = g->vertices()->add("v3");
    auto v4 = g->vertices()->add("v4");
    auto v5 = g->vertices()->add("v5");
    auto v6 = g->vertices()->add("v6");
    auto v7 = g->vertices()->add("v7");
    auto v8 = g->vertices()->add("v8");
    auto v9 = g->vertices()->add("v9");
    
    auto e1 = g->edges()->add(v2, v4);
    auto e2 = g->edges()->add(v3, v4);
    auto e3 = g->edges()->add(v5, v4);
    auto e4 = g->edges()->add(v6, v4);
    auto e5 = g->edges()->add(v5, v7);
    auto e6 = g->edges()->add(v6, v7);
    auto e7 = g->edges()->add(v8, v9);
    
    // Basic terminology
    
    std::cout << "End-vertices of e1: " << (*e1->v1) << ", " << (*e1->v2) << std::endl;
    
    std::cout << "Neighbors of v4: ";
    for (auto v: *g->edges()->neighbors(v4))
    {
        std::cout << (*v) << " ";
    }
    std::cout << std::endl;
    
    std::cout << "Edges incident to v4: ";
    for (auto e: *g->edges()->incident(v4))
    {
        std::cout << "(" << (*e->v1) << "-" << (*e->v2) << ") ";
    }
    std::cout << std::endl;
    
    // Degree
    
    std::cout << "Degrees";
    for (auto v: *g->vertices())
    {
        std::cout << "d(" << (*v) << ")=" << degree(g.get(), v) << " ";
    }
    std::cout << std::endl;
    
    std::cout << "Average degree: " << average_degree(g.get()) << std::endl;
    
    std::cout << "Degree sequence: ";
    for (auto deg: degree_sequence(g.get()))
    {
        std::cout << deg << " ";
    }
    std::cout << std::endl;
    
    std::cout << "Degree distribution: ";
    size_t deg = 0;
    for (auto freq: degree_distribution(g.get()))
    {
        std::cout << "P(" << (deg++) << ")=" << freq << " ";
    }
    std::cout << std::endl;
    
    // Operations
    
    std::cout << "Computing G1 = subgraph(G, {v2, v4, v5})." << std::endl;
    std::vector<const Vertex*> vertices = {v2, v4, v5};
    auto g1 = vertex_induced_subgraph(g.get(), vertices.begin(), vertices.end());
    
    std::cout << "Computing G2 = subgraph(G, {v4, v5, v6})." << std::endl;
    std::vector<const Vertex*> vertices2 = {v4, v5, v6};
    auto g2 = vertex_induced_subgraph(g.get(), vertices2.begin(), vertices2.end());
    
    std::cout << "Computing G3 = G1 union G2." << std::endl;
    auto g3 = graph_union(g1.get(), g2.get());
    
    std::cout << "Computing G4 = G1 intersect G2." << std::endl;
    auto g4 = graph_intersection(g1.get(), g2.get());
    
    std::cout << "Computing complement(G1)." << std::endl;
    auto g5 = graph_complement(g1.get());
    
    std::cout << "Subdividing edge (v8-v9) with vertex v10 in G." << std::endl;
    auto v10 = edge_subdivision(g.get(), e7, "v10");
    auto e8 = g->edges()->get(v8, v10);
    auto e9 = g->edges()->get(v10, v9);
    
    std::cout << "Contracting edge (v9-v10) into vertex v11 in G." << std::endl;
    edge_contraction(g.get(), e9, "v11");

    // Walks, trails, paths
    
    Walk walk(v2);
    walk.extend(e1);
    walk.extend(e4);
    walk.extend(e4);
    walk.extend(e2);
    std::cout << "A walk: " << walk << std::endl;

    Trail trail(v2);
    trail.extend(e1);
    trail.extend(e3);
    trail.extend(e5);
    trail.extend(e6);
    trail.extend(e4);
    trail.extend(e2);
    std::cout << "A trail: " << trail << std::endl;

    Path path(v4);
    path.extend(e3);
    path.extend(e5);
    path.extend(e6);
    path.extend(e4);
    std::string cycle = path.is_cycle()?"yes":"no";
    std::cout << "A path: " << path << " (cycle: " << cycle << ")" << std::endl;
    
    return 0;
}

