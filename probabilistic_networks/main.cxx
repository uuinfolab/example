/**
 * This program shows how to read and print basic statistics about probabilistic networks.
 *
 * History: created on 2020-01-28.
 */

#include <iostream>

#include "io/read_probabilistic_network.hpp"
#include "measures/order.hpp"
#include "measures/degree.hpp"
#include "measures/exp_degree.hpp"
#include "measures/betweenness.hpp"

std::vector<std::vector<double>>
to_adjacency_matrix(
    const uu::net::ProbabilisticNetwork* net
    )
{
    std::vector<std::vector<double>> A;
    size_t order = uu::net::order(net);
    
    // initialize A to a null matrix
    for (size_t row = 0; row < order; row++)
    {
        A.push_back(std::vector<double>(order, 0.0));
    }
    
    auto vertices = net->vertices();
    bool directed = net->is_directed();
    for (auto e: *net->edges())
    {
        size_t row = vertices->index_of(e->v1);
        size_t col = vertices->index_of(e->v2);
        auto attr_value = net->get_prob(e);
        
        double p = attr_value.null ? 0.0 : attr_value.value;
        A[row][col] = p;
        
        if (!directed)
        {
            A[col][row] = p;
        }
    }
    
    return A;
}


int
main()
{
    const std::string net_name = "../input.net";
    
    // reading a probabilistic network from file
    
    auto g = uu::net::read_probabilistic_network(net_name, "net", ',');
    
    // basic statistics
    
    std::cout << "order: " << uu::net::order(g.get()) << std::endl;
    std::cout << "size: " << uu::net::size(g.get()) << std::endl;
    
    // incident edges of v1 and their probabilities
    
    auto v = g->vertices()->get("v1");
    
    for (auto edge: *g->edges()->incident(v, uu::net::EdgeMode::INOUT))
    {
        std::cout << edge->to_string() << " p: " << g->get_prob(edge) << std::endl;
    }
    
    // probabilistic and deterministic measures of v1
    
    auto deg = uu::net::degree(g.get(), v, uu::net::EdgeMode::INOUT);
    std::cout << "Degree (v1): " << deg << std::endl;
    
    auto exp = uu::net::exp_degree(g.get(), v, uu::net::EdgeMode::INOUT);
    std::cout << "Exp. degree (v1): " << exp << std::endl;
    
    auto bet = uu::net::betweenness(g.get());
    std::cout << "Betweenness (v1): " << bet.at(v) << std::endl;
    
    // test conversion to adjacency matrix
    auto A = to_adjacency_matrix(g.get());
    
    std::cout << "Adjacency matrix: " << std::endl;
    for (auto row: A)
    {
        for (double el: row)
        {
            std::cout << el << "\t";
        }
        std::cout << std::endl;
    }
    
    return 0;
}

