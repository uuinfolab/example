This example shows how to read a probabilistic network from file and compute basic measures.

The uunet library must be first installed from https://bitbucket.org/uuinfolab/example/src/master/

Then run:

    mkdir build
    cd build
    cmake ..
    make
	
The example program in main.cxx can be run as:

    ./example
  