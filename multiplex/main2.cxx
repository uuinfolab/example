

#include <iostream>
#include <fstream>
#include <string>
#include "io/read_multilayer_network.hpp"
#include "io/read_multilayer_communities.hpp"
#include "io/write_multilayer_communities.hpp"
#include "community/glouvain2.hpp"
#include "operations/flatten.hpp"
#include <memory>
#include <unordered_map>
#include <string>
#include <vector>
#include <ostream>
#include "embeddings/EmbeddingAlgorithms.hpp"
#include "community/normalized_mutual_information.hpp"
#include <vector>

int main()
{
    std::ifstream infile("../Simulations2.txt");
    std::ofstream resfile("../Mutual_results.txt");

    std::vector<int> algoNum1;
    std::vector<std::string> input1, compare1;
    int clusters;
    int iterationsEACH;
    double value;

    std::vector<float> r1, p1, q1;
    std::vector<int> size_of_embedding1, len_rand_walk1, numb_rand_walks1, clusters1, window1, cluster_iterations1, k_min1, k_max1;
    std::vector<std::string> alpha1, sample1;
    std::vector<int> iterations1, min_word_freq1, negative_sample1, threads1;

    bool verbose = false;
    std::vector<std::string> lswap_metric_string1;

    std::string str;
    std::string token;
    int clock = 0;

    while (std::getline(infile, str))
    {
        std::istringstream iss(str);

        while (getline(iss, token, ' '))
        {

            if (clock == 0)
            {
                algoNum1.push_back(std::stoi(token));
                std::cout << "ALGO NUM " << token << " ";
            }

            if (clock == 1)
            {
                lswap_metric_string1.push_back(token);
                std::cout << token << " ";
            }

            if (clock == 2)
            {
                input1.push_back(token);
                std::cout << token << " ";
            }

            if (clock == 3)
            {
                compare1.push_back(token);
                std::cout << token << " ";
            }
            if (clock == 4)
            {
                r1.push_back(std::stof(token));
                std::cout << token << " ";
            }

            if (clock == 5)
            {
                p1.push_back(std::stof(token));
                std::cout << token << " ";
            }

            if (clock == 6)
            {
                q1.push_back(std::stof(token));
                std::cout << token << " ";
            }

            if (clock == 7)
            {
                size_of_embedding1.push_back(std::stoi(token));
                std::cout << token << " ";
            }
            if (clock == 8)
            {
                len_rand_walk1.push_back(std::stoi(token));
                std::cout << token << " ";
            }

            if (clock == 9)
            {
                numb_rand_walks1.push_back(std::stoi(token));
                std::cout << token << " ";
            }

            if (clock == 10)
            {
                k_min1.push_back(std::stoi(token));
                std::cout << token << " ";
            }

            if (clock == 11)
            {
                k_max1.push_back(std::stoi(token));
                std::cout << token << " ";
            }

            if (clock == 12)
            {
                window1.push_back(std::stoi(token));
                std::cout << token << " ";
            }

            if (clock == 13)
            {
                cluster_iterations1.push_back(std::stoi(token));
                std::cout << token << " ";
            }

            if (clock == 14)
            {
                alpha1.push_back(token);
                std::cout << token << " ";
            }

            if (clock == 15)
            {
                sample1.push_back(token);
                std::cout << token << " ";
            }

            if (clock == 16)
            {
                iterations1.push_back(std::stoi(token));
                std::cout << token << " ";
            }

            if (clock == 17)
            {
                min_word_freq1.push_back(std::stoi(token));
                std::cout << token << " ";
            }

            if (clock == 18)
            {
                negative_sample1.push_back(std::stoi(token));
                std::cout << token << " ";
            }

            if (clock == 19)
            {
                threads1.push_back(std::stoi(token));
                std::cout << token << " ";
            }

            if (clock == 20)
            {
                iterationsEACH = std::stoi(token);
                std::cout << iterationsEACH << std::endl;
            }
        }
        std::cout << std::endl;
        str.clear();
        clock++;
    }

    for (auto input : input1)
    {
        auto m_not_aligned = uu::net::read_attributed_homogeneous_multilayer_network(input, "aucs", ',', false);
        int norm_const_not_aligned = 0;
        for (auto l : *m_not_aligned->layers())
        {
            norm_const_not_aligned += l->vertices()->size();
        }
        auto m_aligned = uu::net::read_attributed_homogeneous_multilayer_network(input, "aucs", ',', true);
        int norm_const_aligned = m_aligned->layers()->size() * m_aligned->actors()->size();
        for (auto algoNum : algoNum1)
        {
            for (auto lswap_metric_string : lswap_metric_string1)
            {
                for (auto compare : compare1)
                {
                    for (auto r : r1)
                    {
                        for (auto p : p1)
                        {
                            for (auto q : q1)
                            {
                                for (auto size_of_embedding : size_of_embedding1)
                                {
                                    for (auto len_rand_walk : len_rand_walk1)
                                    {
                                        for (auto numb_rand_walks : numb_rand_walks1)
                                        {
                                            for (auto window : window1)
                                            {
                                                for (auto cluster_iterations : cluster_iterations1)
                                                {
                                                    for (auto k_min : k_min1)
                                                    {
                                                        for (auto k_max : k_max1)
                                                        {
                                                            for (auto alpha : alpha1)
                                                            {
                                                                for (auto sample : sample1)
                                                                {
                                                                    for (auto iterations : iterations1)
                                                                    {

                                                                        for (auto min_word_freq : min_word_freq1)
                                                                        {

                                                                            for (auto negative_sample : negative_sample1)
                                                                            {

                                                                                for (auto threads : threads1)
                                                                                {

                                                                                    for (int xxx = 0; xxx < iterationsEACH; xxx++)
                                                                                    {

                                                                                        double mutual_info = 0.0;

                                                                                        if (algoNum == 1)
                                                                                        {

                                                                                            auto embedding = uu::net::algo_1_MG(m_not_aligned.get(), p, q, size_of_embedding,
                                                                                                                                len_rand_walk, numb_rand_walks, clusters,
                                                                                                                                window, cluster_iterations, alpha, sample,
                                                                                                                                iterations, min_word_freq, negative_sample, threads /*, verbose */);
                                                                                            auto kmeans = uu::net::KMeans(k_min, k_max, m_not_aligned.get(), cluster_iterations, embedding);
                                                                                            clusters = kmeans.K_clusters_best.size();
                                                                                            //kmeans.print_clusters();
                                                                                            auto ground = uu::net::read_multilayer_communities(compare, m_not_aligned.get(), ',');

                                                                                            mutual_info = uu::net::normalized_mutual_information(ground, kmeans.communities, norm_const_not_aligned);
                                                                                        }

                                                                                        else if (algoNum == 2)
                                                                                        {

                                                                                            auto embedding = uu::net::algo_2_MG(m_aligned.get(), p, q, size_of_embedding,
                                                                                                                                len_rand_walk, numb_rand_walks, clusters,
                                                                                                                                window, cluster_iterations, alpha, sample,
                                                                                                                                iterations, min_word_freq, negative_sample, threads);

                                                                                            auto kmeans = uu::net::KMeans(k_min, k_max, m_aligned.get(), cluster_iterations, embedding);
                                                                                            clusters = kmeans.K_clusters_best.size();
                                                                                            auto ground = uu::net::read_multilayer_communities(compare, m_aligned.get(), ',');
                                                                                            // kmeans.print_clusters();

                                                                                            mutual_info = uu::net::normalized_mutual_information(ground, kmeans.communities, norm_const_aligned);
                                                                                        }
                                                                                        else if (algoNum == 3)
                                                                                        {

                                                                                            auto embedding = uu::net::algo_3_MG(m_not_aligned.get(), r, p, q, size_of_embedding,
                                                                                                                                len_rand_walk, numb_rand_walks, clusters,
                                                                                                                                window, cluster_iterations, alpha, sample,
                                                                                                                                iterations, min_word_freq, negative_sample, threads);

                                                                                            auto kmeans = uu::net::KMeans(k_min, k_max, m_not_aligned.get(), cluster_iterations, embedding);
                                                                                            // kmeans.print_clusters();
                                                                                            clusters = kmeans.K_clusters_best.size();
                                                                                            auto ground = uu::net::read_multilayer_communities(compare, m_not_aligned.get(), ',');

                                                                                            mutual_info = uu::net::normalized_mutual_information(ground, kmeans.communities, norm_const_not_aligned);
                                                                                        }
                                                                                        else if (algoNum == 4)
                                                                                        {

                                                                                            uu::net::LayerSwapMetric lswap_metric;
                                                                                            if (lswap_metric_string == "LAYERS_JACC_SIM")
                                                                                            {
                                                                                                lswap_metric = uu::net::LayerSwapMetric::LAYERS_JACC_SIM;
                                                                                            }
                                                                                            else if (lswap_metric_string == "LAYERS_JACC_DIST")
                                                                                            {
                                                                                                lswap_metric = uu::net::LayerSwapMetric::LAYERS_JACC_DIST;
                                                                                            }
                                                                                            else if (lswap_metric_string == "NEIGH_JACC_SIM")
                                                                                            {
                                                                                                lswap_metric = uu::net::LayerSwapMetric::NEIGH_JACC_SIM;
                                                                                            }
                                                                                            else if (lswap_metric_string == "NEIGH_JACC_DIST")
                                                                                            {
                                                                                                lswap_metric = uu::net::LayerSwapMetric::NEIGH_JACC_DIST;
                                                                                            }
                                                                                            auto embedding = uu::net::algo_3_MG_new(m_not_aligned.get(), lswap_metric, r, p, q, size_of_embedding,
                                                                                                                                    len_rand_walk, numb_rand_walks, clusters,
                                                                                                                                    window, cluster_iterations, alpha, sample,
                                                                                                                                    iterations, min_word_freq, negative_sample, threads);

                                                                                            auto kmeans = uu::net::KMeans(k_min, k_max, m_not_aligned.get(), cluster_iterations, embedding);
                                                                                            // kmeans.print_clusters();

                                                                                            auto ground = uu::net::read_multilayer_communities(compare, m_not_aligned.get(), ',');
                                                                                            clusters = kmeans.K_clusters_best.size();
                                                                                            mutual_info = uu::net::normalized_mutual_information(ground, kmeans.communities, norm_const_not_aligned);
                                                                                        }
                                                                                        else if (algoNum == 5) {
                                                                                            auto embedding = uu::net::algo_1_MG_unweighted(m_not_aligned.get(), p, q, size_of_embedding,
                                                                                                                                len_rand_walk, numb_rand_walks, clusters,
                                                                                                                                window, cluster_iterations, alpha, sample,
                                                                                                                                iterations, min_word_freq, negative_sample, threads /*, verbose */);
                                                                                            auto kmeans = uu::net::KMeans(k_min, k_max, m_not_aligned.get(), cluster_iterations, embedding);
                                                                                            clusters = kmeans.K_clusters_best.size();
                                                                                            //kmeans.print_clusters();
                                                                                            auto ground = uu::net::read_multilayer_communities(compare, m_not_aligned.get(), ',');

                                                                                            mutual_info = uu::net::normalized_mutual_information(ground, kmeans.communities, norm_const_not_aligned);
                                                       
                                                                                        }

                                                                                        // value += sumnumber;
                                                                                        resfile << algoNum << " " << lswap_metric_string << " " << input << " " << compare << " " << r << " " << p << " " << q << " " << size_of_embedding << " " << len_rand_walk << " " << numb_rand_walks << " " << k_min << " " << k_max << " " << window << " " << cluster_iterations << " " << alpha << " " << sample << " " << iterations << " " << min_word_freq << " " << negative_sample << " " << threads << " ";
                                                                                        resfile << " RESULT " << mutual_info << " CLUSTERS:" << clusters << std::endl;
                                                                                        // resfile << " avgValue " << value<< td::endl;
                                                                                    }
                                                                                    // value /= iterationsEACH;
                                                                                    // resfile << algoNum << " " << lswap_metric_string << " " << input << " " << compare << " " << r << " " << p << " " << q << " " << size_of_embedding << " " << len_rand_walk << " " << numb_rand_walks << " " << k_min << " " << k_max << " " << window << " " << cluster_iterations << " " << alpha << " " << sample << " " << iterations << " " << min_word_freq << " " << negative_sample << " " << threads << " ";
                                                                                    // resfile << " RESULT " << value << " CLUSTERS:" << clusters << std::endl;
                                                                                    // value = 0;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    // while(getline(iss, token,  ' ')) {
    //     algoNum.push_back(std::stoi( token ));

    //     std::cout << token  << std::endl;
    // }

    // for (auto x : algoNum) {
    //     std::cout << x << std::endl;
    // }

    // for (auto x : lswap_metric_string) {
    //     std::cout << x << std::endl;
    // }
}