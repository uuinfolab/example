This example reads a multiplex network and iterates across its layers. Every layer represents a different type of edges among the same set of vertices.

After you have installed the uunet library (present as a different repository on this site) you can run this example as follows:

```
mkdir build
cd build
cmake ..
make
./example
```
