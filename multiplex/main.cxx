
#include <iostream>
#include <fstream>
#include <string>
#include "io/read_multilayer_network.hpp"
#include "io/read_multilayer_communities.hpp"
#include "io/write_multilayer_communities.hpp"
#include "community/glouvain2.hpp"
#include "operations/flatten.hpp"
#include <memory>
#include <unordered_map>
#include <string>
#include <vector>
#include <ostream>
#include "embeddings/EmbeddingAlgorithms.hpp"
#include "community/normalized_mutual_information.hpp"
#include "Directed_Test_Net.cxx"

int main()
{

    std::ifstream infile("../Simulations.txt");
    std::ofstream resfile("../Mutual_results.txt");

    int algoNum;
    std::string input, compare;
    float r, p, q;
    int size_of_embedding, len_rand_walk, numb_rand_walks, clusters, window, cluster_iterations, k_min, k_max;
    std::string alpha, sample;
    int iterations, min_word_freq, negative_sample, threads;
    bool verbose;
    std::string lswap_metric_string;

    while (infile >> algoNum >> lswap_metric_string >>input >> compare >> r >> p >> q >> size_of_embedding >> len_rand_walk >> numb_rand_walks >>
           k_min >> k_max >> window >> cluster_iterations >> alpha >> sample >> iterations >>
           min_word_freq >> negative_sample >> threads)
    {

        if (algoNum == 1) {
            auto mm = uu::net::read_attributed_homogeneous_multilayer_network(input, "aucs", ',', false);
            auto layers = mm->layers()->size();
            auto actors_size = mm->actors()->size();

            auto embedding = uu::net::algo_1_MG(mm.get(), p, q, size_of_embedding,
                                                len_rand_walk, numb_rand_walks, clusters,
                                                window, cluster_iterations, alpha, sample,
                                                iterations, min_word_freq, negative_sample, threads /*, verbose */);
            auto kmeans = uu::net::KMeans(k_min, k_max, mm.get(), cluster_iterations, embedding);
            kmeans.print_clusters();
            kmeans.print_cluster_data_to_file("data_for_plotting_2d_toy.txt");
            //auto ground = uu::net::read_multilayer_communities(compare, m.get(), ',');

            //auto sumnumber = uu::net::normalized_mutual_information(ground, kmeans.communities, layers * actors_size);
            //resfile << algoNum << " ";
            //resfile << sumnumber << std::endl;
        }

        else if (algoNum == 2) 
        {
            auto mm = uu::net::read_attributed_homogeneous_multilayer_network(input, "aucs", ',', true);

            auto layers_size = mm->layers()->size();
            auto actors_size = mm->actors()->size();
            std::cout << "asd\n";
            auto embedding = uu::net::algo_2_MG(mm.get(), p, q, size_of_embedding,
                                                len_rand_walk, numb_rand_walks, clusters,
                                                window, cluster_iterations, alpha, sample,
                                                iterations, min_word_freq, negative_sample, threads /*, verbose */);

            auto kmeans = uu::net::KMeans(k_min, k_max, mm.get(), cluster_iterations, embedding);
            //auto ground = uu::net::read_multilayer_communities(compare, m.get(), ',');
            kmeans.print_clusters();

            //auto sumnumber = uu::net::normalized_mutual_information(ground, kmeans.communities, layers_size * actors_size);
            //resfile << algoNum << " ";
            //resfile << sumnumber << std::endl;
        }
        else if (algoNum == 3)
        {
            auto mm = uu::net::read_attributed_homogeneous_multilayer_network(input, "aucs", ',', false);
            auto layers_size = mm->layers()->size();
            auto actors_size = mm->actors()->size();

            auto embedding = uu::net::algo_3_MG(mm.get(), r, p, q, size_of_embedding,
                                                len_rand_walk, numb_rand_walks, clusters,
                                                window, cluster_iterations, alpha, sample,
                                                iterations, min_word_freq, negative_sample, threads /*, verbose */);
           

            auto kmeans = uu::net::KMeans(k_min, k_max, mm.get(), cluster_iterations, embedding);
            kmeans.print_clusters();

            //auto ground = uu::net::read_multilayer_communities(compare, m.get(), ',');

            //auto sumnumber = uu::net::normalized_mutual_information(ground, kmeans.communities, layers_size * actors_size);
            //resfile << algoNum << " ";
            //resfile <<  sumnumber << std::endl;
        }
        else if (algoNum == 4)
        {
            auto mm = uu::net::read_attributed_homogeneous_multilayer_network(input, "aucs", ',', false);
            auto layers_size = mm->layers()->size();
            auto actors_size = mm->actors()->size();
            uu::net::LayerSwapMetric lswap_metric;
            if(lswap_metric_string == "LAYERS_JACC_SIM") {
                lswap_metric = uu::net::LayerSwapMetric::LAYERS_JACC_SIM;
            }
            else if(lswap_metric_string == "LAYERS_JACC_DIST") {
                lswap_metric = uu::net::LayerSwapMetric::LAYERS_JACC_DIST;
            }
            else if(lswap_metric_string == "NEIGH_JACC_SIM") {
                lswap_metric = uu::net::LayerSwapMetric::NEIGH_JACC_SIM;
            }
            else if(lswap_metric_string == "NEIGH_JACC_DIST") {
                lswap_metric = uu::net::LayerSwapMetric::NEIGH_JACC_DIST;

            }
            auto embedding = uu::net::algo_3_MG_new(mm.get(),lswap_metric, r, p, q, size_of_embedding,
                                                len_rand_walk, numb_rand_walks, clusters,
                                                window, cluster_iterations, alpha, sample,
                                                iterations, min_word_freq, negative_sample, threads /*, verbose */);

            auto kmeans = uu::net::KMeans(k_min, k_max, mm.get(), cluster_iterations, embedding);
            kmeans.print_clusters();

            //auto ground = uu::net::read_multilayer_communities(compare, m.get(), ',');

            //auto sumnumber = uu::net::normalized_mutual_information(ground, kmeans.communities, layers_size * actors_size);
            //resfile << algoNum << " ";
            //resfile <<  sumnumber << std::endl;
        }

    }
    
    
        infile.close();
        resfile.close();

        return 0;
    
}