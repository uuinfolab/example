#include <iostream>
#include <fstream>
#include <string>
#include "io/read_multilayer_network.hpp"
#include "io/read_multilayer_communities.hpp"
#include "io/write_multilayer_communities.hpp"
#include "community/glouvain2.hpp"
#include "operations/flatten.hpp"
#include <memory>
#include <unordered_map>
#include <string>
#include <vector>
#include <ostream>
#include "embeddings/EmbeddingAlgorithms.hpp"
#include "community/normalized_mutual_information.hpp"
class Directed_Test_Net
{
public:
  static std::unique_ptr<uu::net::MultilayerNetwork> Directed_Net()
  {
    std::unique_ptr<uu::net::MultilayerNetwork> net;
    //const uu::net::Vertex *v1, *v2, *v3, *v4, *v5, *v6, *v7, *v8;
    //const uu::net::Network *l1, *l2, *l3, *l4;

    net = std::make_unique<uu::net::MultilayerNetwork>("net");
    // Adding actors

    auto v1 = net->actors()->add("v1");
    auto v2 = net->actors()->add("v2");
    auto v3 = net->actors()->add("v3");
    auto v4 = net->actors()->add("v4");
    auto v5 = net->actors()->add("v5");
    auto v6 = net->actors()->add("v6");
    auto v7 = net->actors()->add("v7");
    auto v8 = net->actors()->add("v8");

    // Adding layers

    auto l1 = net->layers()->add(std::make_unique<uu::net::Network>("l1", uu::net::EdgeDir::DIRECTED));
    auto l2 = net->layers()->add(std::make_unique<uu::net::Network>("l2", uu::net::EdgeDir::DIRECTED));
    auto l3 = net->layers()->add(std::make_unique<uu::net::Network>("l2", uu::net::EdgeDir::DIRECTED));

    // Adding vertices to layers

    for (auto actor : *net->actors())
    {
      l1->vertices()->add(actor);
      l2->vertices()->add(actor);
      l3->vertices()->add(actor);
    }

    // Adding edges

    l1->edges()->add(v1, v2);
    l1->edges()->add(v1, v3);
    l1->edges()->add(v2, v3);
    l1->edges()->add(v4, v5);
    l1->edges()->add(v4, v6);
    l1->edges()->add(v5, v6);

    l2->edges()->add(v1, v2);
    l2->edges()->add(v1, v3);
    l2->edges()->add(v2, v3);
    l2->edges()->add(v4, v5);
    l2->edges()->add(v4, v6);
    l2->edges()->add(v5, v6);

    l3->edges()->add(v7, v6);
    l3->edges()->add(v8, v7);
    l3->edges()->add(v8, v5);
    l3->edges()->add(v8, v6);
    return std::move(net);
  }
};