/**
 * Basic graph operations.
 *
 * History: created on 2018-12-21.
 */

#include "io/read_network.hpp" // read_network
#include "measures/degree.hpp" // degree, degree_sequence, degree_distribution, average_degree
#include "algorithms/components.hpp" // components
#include "utils/summary.hpp" // summary_short

int
main()
{
    
    const std::string network_file = "/Users/matteomagnami/Downloads/edge_list_followers";
    
    auto g = uu::net::read_network(network_file, "simple_network", ' ');
    
    std::cout << "G: " << uu::net::summary_short(g.get()) << std::endl;
    
    auto comp = components(g.get());
    
    std::map<size_t, size_t> stats;
    
    for (auto c_id: comp)
    {
        auto it = stats.find(c_id);
        
        if (it == stats.end())
            stats[c_id] = 0;
        else stats[c_id]++;
    }
    
    for (auto pair: stats)
    {
        std::cout << pair.second << std::endl;
    }
    
    /*
    // Getting vertices
    
    auto v2 = g->vertices()->get("vertex-name");
    
    auto neigh = g->edges()->neighbors(v4);
    std::cout << "Neighbors of v4: ";
    for (auto v: *neigh)
    {
        std::cout << (*v) << " ";
    }
    std::cout << std::endl;
    
    // Computing vertex degrees
    
    auto avg_deg = uu::net::average_degree(g.get());
    std::cout << "Average degree: " << avg_deg << std::endl;
    
    std::cout << "Degree sequence: ";
    auto deg_seq = uu::net::degree_sequence(g.get());
    for (auto deg: deg_seq)
    {
        std::cout << deg << " ";
    }
    std::cout << std::endl;
    
    std::cout << "Degree distribution: ";
    auto deg_distr = uu::net::degree_distribution(g.get());
    for (auto freq: deg_distr)
    {
        std::cout << freq << " ";
    }
    std::cout << std::endl;
    
    /*
    for (auto v: *g->vertices())
    {
        size_t deg = uu::net::degree(g.get(), v);
        std::cout << "deg(" << (*v) << "): " << deg << std::endl;
    }
    */
    
    return 0;
}

