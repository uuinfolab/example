This example reads a network from file and performs a selection of operations on it, including the computation of basic statistics and graph transformations.

The uunet library must be installed first. Then run:

mkdir build
cd build
cmake ..
make
./example
