/**
 * This file shows how to read a temporal network from file,
 * iterate through its elements, discretize it into an
 * ordered multiplex network and discover communities in it.
 *
 * History: created on 2018-09-10.
 */

#include "io/read_temporal_network.hpp"
#include "community/optimize_slices.hpp"

int
main(
    int argc,
    char *argv[]
)
{

    if (argc < 3)
    {
        std::cout << "usage: " << argv[0] << " <temporal-network-file> <number-of-slices>" << std::endl;
        std::cout << "example: " << argv[0] << " ../data/synthetic_2cliques.tnt 30" << std::endl;
        return 0;
    }
        
    std::string data = std::string(argv[1]);
    size_t max_num_slices = std::stoi(argv[2]);
    auto net = uu::net::read_temporal_network(data, "t-net", ' ');
    
    // list of normalized modularities for each number of slices
    auto modularities = uu::net::optimize_slices(net.get(), max_num_slices);
    
    size_t num_slices = 1;
    for (auto mod: modularities)
    {
        std::cout << num_slices++ << " " << mod << std::endl;
    }

    return 0;
}

