#ifndef UU_MNET_TEST_H_
#define UU_MNET_TEST_H_

#include <unordered_map>
#include <map>
#include <numeric>
#include <utility>
#include <cstdlib>
#include "net/datastructures/objects/EdgeMode.h"

namespace uu {
namespace net {


    template <typename M, typename CS>
    std::pair<double,double>
    mod1(const M* mnet, const CS* communities, double omega)
    {
        
        // only one slice...
        
        auto res = std::make_pair(0.0,0.0);
        double mu = 0;
        
        std::unordered_map<std::string,int> m_s;
        
        for (auto s: *mnet->layers())
        {
            double m = s->edges()->size();
            
            if (!s->is_directed())
            {
                m *= 2;
            }
            
            // FIX TO THE ORIGINAL EQUATION WHEN THERE ARE NO EDGES
            if (m == 0)
            {
                m = 1;    // no effect on the formula
            }
            
            m_s[s->name] = m;
            mu += m;
        }
        
        // quick fix
        std::map<const uu::net::Network*,size_t> layer_index; // FIX ME!!
        for (size_t l_idx=0; l_idx<mnet->layers()->size(); l_idx++)
        {
            layer_index[mnet->layers()->at(l_idx)] = l_idx;
        }
        
        for (auto community: *communities)
        {
            for (auto i: *community)
            {
                for (auto j: *community)
                {
                    if (i==j)
                    {
                        continue;    // not in the original definition - we do this assuming to deal with simple graphs
                    }
                    
                        //std::cout << i->to_string() << " " << groups.count(i) << std::endl;
                        //std::cout << j->to_string() << " " << groups.count(j) << std::endl;
                    
                        // same layer
                    if (i.second==j.second)
                    {
                        //auto v1 = i.second->vertices()->get(i.first);
                        //auto v2 = j.second->vertices()->get(j.first);
                        auto v1 = i.first;
                        auto v2 = j.first;
                        
                        
                        long k_i = i.second->edges()->neighbors(v1,EdgeMode::OUT)->size();
                        long k_j = j.second->edges()->neighbors(v2,EdgeMode::IN)->size();
                        int a_ij = i.second->edges()->get(v1, v2)? 1.0 : 0.0;
                        
                        res.first += a_ij;
                        res.second += (double)k_i * k_j / (m_s.at(i.second->name));
                        
                    }
                    
                    if (i.first==j.first)
                    {
                        if (std::abs((int)(layer_index[i.second]-layer_index[j.second]))<2)
                        {
                            std::cout << "never here..." << std::endl;
                        }
                    }
                }
            }
            
                //std::cout << "->" << m_net << std::endl;
        }
        
            //std::cout << "same" << std::endl;
        
            //std::cout << mu << std::endl;
        int num_layers = mnet->layers()->size();
        int num_actors = mnet->vertices()->size();
        
        mu += 2*(num_layers-1)*num_actors*omega; // unclear if we should multiply by c
        
        //std::cout << mu << std::endl;
        
        return res;
    }
    
    
    
    template <typename M>
    std::unique_ptr<CommunityStructure<VertexLayerCommunity<const typename M::layer_type>>>
    read_ground_truth(
                      const std::string& infile, char separator, const M* mnet
                      )
    {
        
        size_t num_layers = mnet->layers()->size();
        size_t num_actors = mnet->vertices()->size();
        
            // group by community id
        
        core::CSVReader csv;
        csv.trim_fields(true);
        csv.set_field_separator(separator);
        csv.open(infile);
        std::unordered_map<std::string, std::list<std::pair<const Vertex*, const typename M::layer_type*>> > result;
        
        while (csv.has_next()) {
            std::vector<std::string> v = csv.get_next();
            auto a = mnet->vertices()->get(v.at(0));
            if (v.size()==3) {
                auto l = mnet->layers()->get(v.at(1));
                auto iv = std::make_pair(a, l);
                result[v.at(2)].push_back(iv);
            }
            else if (v.size()==2) {
                for (auto l: *mnet->layers()) {
                    auto iv = std::make_pair(a, l);
                    result[v.at(1)].push_back(iv);
                }
            }
            else throw uu::core::WrongFormatException("The ground truth file must specify Actor,Layer,CommunityID or Actor,CommunityID");
        }
        
        
        
            // build community structure
        
        auto communities = std::make_unique<CommunityStructure<VertexLayerCommunity<const typename M::layer_type>>>();
        
        for (auto pair: result)
        {
            auto c = std::make_unique<VertexLayerCommunity<const typename M::layer_type>>();
            
            for (auto vertex_layer_pair: pair.second)
            {
                c->add(vertex_layer_pair);
            }
            
            communities->add(std::move(c));
        }
        
        return communities;
    }
    

    
    
    template <typename CS>
    double
    nmi(
                                  const CS& com1,
                                  const CS& com2,
                                  int n
                                  )
    {
        double entropy_c1 = 0;
        
        for (auto community: *com1)
        {
            int size1 = community->size();
            
            if (size1 == 0)
            {
                continue;
            }
            
            entropy_c1 -= (double)size1/n * std::log2((double)size1/n);
        }
        
        double entropy_c2 = 0;
        
        for (auto community: *com2)
        {
            int size2 = community->size();
            
            if (size2 == 0)
            {
                continue;
            }
            
            entropy_c2 -= (double)size2/n * std::log2((double)size2/n);
        }
        
        double info = 0;
        
        for (auto community1: *com1)
        {
            for (auto community2: *com2)
            {
                    // intersection - @todo call some existing function...
                size_t common_nodes = 0;
                
                for (auto v: *community1)
                {
                    if (community2->contains(v))
                    {
                        common_nodes++;
                    }
                }
                
                int size1 = community1->size();
                int size2 = community2->size();
                
                if (size1==0 || size2==0 || common_nodes==0)
                {
                    continue;
                }
                
                info += (double)common_nodes/n * std::log2((double)n*common_nodes/(size1*size2));
            }
        }
        
        return info/((entropy_c1+entropy_c2)/2);
    }

}
}

#endif
